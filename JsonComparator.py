#!/usr/bin/env python3

from datetime import datetime
import sys
import threading
import re


def debug(fn_debug):
    def wrapper(*args):
        d1 = datetime.now()
        print("Begin :", fn_debug.__qualname__)
        r = fn_debug(*args)
        print("Returning :", fn_debug.__qualname__, "=>", r, "======", (datetime.now() - d1).total_seconds())
    return wrapper


# Test
class JsonComparator:

    def __init__(self, threshold=0.0001, to_be_ignored="", sort_key_map=None, test_case_id="", test_case_name="", reporter=None):
        self.to_be_ignored = [re.compile(key.strip().replace("[", "\\[").replace("]", "\\]").replace('"', '\"').replace("*", "[0-9]*").replace("^", ".*")) for key in to_be_ignored]
        self.sort_key_map = sort_key_map
        self.threshold = threshold
        self.test_case_id = test_case_id
        self.test_case_name = test_case_name
        self.reporter = reporter
        self.result = dict()
        self.sort_maps = dict()
        self.compare_attributes = dict()
        self.compare_dict1 = dict()
        self.compare_dict2 = dict()
        self.flat_dicts = dict()
        self.thread_exceptions = set()
        self.is_array_table = False
        self.fields1 = list()
        self.fields2 = list()
        self.comparison_time = 0

    # def __init__(self, threshold=0.0001, report_meta_data_file=None, test_case_id="", test_case_name="", reporter=None, clear_to_be_ignored_variable=True):
    #     self.report_meta_data_file = json.load(open(report_meta_data_file))
    #     if clear_to_be_ignored_variable:
    #         self.clear_to_be_ignored_variable()
    #     report_key = self.report_meta_data_file[test_case_name]
    #     self.sort_key_map = report_key["sort_key_map"]
    #     to_be_ignored = []
    #     if "Common" in self.report_meta_data_file.keys():
    #         to_be_ignored.extend(self.report_meta_data_file["Common"]["to_be_ignored_static"])
    #     if "to_be_ignored_static" in report_key.keys():
    #         to_be_ignored.extend(report_key["to_be_ignored_static"])
    #     if "to_be_ignored_variable" in report_key.keys():
    #         to_be_ignored.extend(report_key["to_be_ignored_variable"])
    #     self.to_be_ignored = [re.compile(key.strip().replace("[", "\\[").replace("]", "\\]").replace('"', '\"').replace("*", "[0-9]*").replace("^", ".*")) for key in to_be_ignored]
    #     self.threshold = threshold
    #     self.test_case_id = test_case_id
    #     self.test_case_name = test_case_name
    #     self.reporter = reporter
    #     self.result = dict()
    #     self.sort_maps = dict()
    #     self.compare_attributes = dict()
    #     self.compare_dict1, self.compare_dict2 = dict(), dict()
    #     self.flat_dicts = dict()
    #     self.thread_exceptions = set()
    #     self.is_array_table = False
    #     self.fields1, self.fields2 = list(), list()
    #     self.comparison_time = 0

    def __str__(self):
        return "To Be Ignored ==> {}\n\nThreshold ==> {}\n\nSort Key Map ==> {}".format(self.to_be_ignored,
                                                                                        self.threshold,
                                                                                        self.sort_key_map)

    @staticmethod
    def is_empty(compare_obj):
        return not any(value for value in compare_obj.values())

    @staticmethod
    def transform_reporting_differences(value):
        if not value:
            return ""
        value = str(value)
        value = value.replace("{'", "")
        value = value.replace("'}", "")
        value = value.replace("', '", "; ")
        return value

    @staticmethod
    def __get_value(d, key):
        split_key = key.split(".")
        for k in split_key:
            val = d.get(k, None)
            if not val:
                return "NA"
            elif isinstance(val, list):
                if isinstance(val, dict):
                    raise Exception("Key - {} not found or not in correct format".format(k)).with_traceback(ke.__traceback__)
                return val
            elif isinstance(val, dict):
                d = val
            else:
                return val

    @staticmethod
    def transform_value(value):
        p = re.compile("[0-9|a-z]{8}-[0-9|a-z]{4}-[0-9|a-z]{4}-[0-9|a-z]{4}-[0-9|a-z]{12}")
        if p.match(str(value)):
            return True
        return value

    def clear_to_be_ignored_variable(self):
        modified_report_meta_data_file = self.report_meta_data_file
        for report_key, report_key_value in self.report_meta_data_file.items():
            if "to_be_ignored_variable" in report_key_value.keys():
                modified_report_meta_data_file[report_key]["to_be_ignored_variable"] = []
        self.report_meta_data_file = modified_report_meta_data_file

    def does_dict_contain_list_value(self, obj):
        found = False
        for k, v in obj.items():
            if isinstance(v, list):
                found = True
                break
            elif isinstance(v, dict):
                found = self.does_dict_contain_list_value(v)
                if found:
                    break
        return found

    def flatten_nested_dict(self, obj, pre_key=''):
        if obj:
            if isinstance(obj, dict):
                pre_key = pre_key + '.' if pre_key else pre_key
                for key in obj.keys():
                    yield from self.flatten_nested_dict(obj[key], pre_key + str(key))
            elif isinstance(obj, list) and isinstance(obj[0], dict):
                for i in range(len(obj)):
                    list_key = "[" + str(i) + "]"
                    yield from self.flatten_nested_dict(obj[i], pre_key + list_key)
            else:
                yield pre_key, obj
        else:
            yield pre_key, obj

    def _compare_dict(self, dict1, dict2, matched_keys):
        difference = dict()
        for matched_key in matched_keys:
            value1, value2 = self.transform_value(dict1[matched_key]), self.transform_value(dict2[matched_key])
            if type(value1) != type(value2):
                difference.update({matched_key: "{}, {}".format(
                    str(type(value1)).replace(",", ";").replace("\n", "").replace("\r", ""),
                    str(type(value2)).replace(",", ";").replace("\n", "").replace("\r", ""))})
                continue
            if value1 != value2:
                if isinstance(value1, float) or isinstance(value1, int):
                    if abs(value1 - value2) > float(self.threshold):
                        difference.update({matched_key: "{}, {}".format(str(value1).replace(",", ";"),
                                                                        str(value2).replace(",", ";"))})
                else:
                    difference.update({matched_key: "{}, {}".format(
                        str(value1).replace(",", ";").replace("\n", "").replace("\r", ""),
                        str(value2).replace(",", ";").replace("\n", "").replace("\r", ""))})
        return difference

    def _is_key_to_be_ignored(self, key):
        if "@UUID" in key:
            return True
        for pattern in self.to_be_ignored:
            if pattern.match(key):
                return True
        return False

    def _flatten_dict(self, dict_obj):
        self.flat_dicts[threading.currentThread().getName()] = {key: value for key, value in self.flatten_nested_dict(dict_obj, "")}

    def _compute_matched_lr(self, keys1, keys2):
        # [self.compare_attributes["matched"].add(key) if key in keys2 else self.compare_attributes["lr"].add(key) for key in keys1 if not self._is_key_to_be_ignored(key)]
        for key in keys1:
            if not self._is_key_to_be_ignored(key):
                if key in keys2:
                    self.compare_attributes["matched"].add(key)
                else:
                    self.compare_attributes["lr"].add(key)

    def _compute_rl(self, keys1, keys2):
        # [self.compare_attributes["rl"].add(key) for key in keys2 if key not in keys1 if not self._is_key_to_be_ignored(key)]
        for key in keys2:
            if key not in keys1 and not self._is_key_to_be_ignored(key):
                self.compare_attributes["rl"].add(key)

    def _compare_keys(self, keys1, keys2):
        self.compare_attributes = dict({"lr": set(), "rl": set(), "matched": set()})
        t1 = threading.Thread(target=self._compute_matched_lr, args=(keys1, keys2))
        t2 = threading.Thread(target=self._compute_rl, args=(keys1, keys2))
        t1.start(); t2.start()
        t1.join(); t2.join()
        return self.compare_attributes["lr"], self.compare_attributes["rl"], self.compare_attributes["matched"]

    def update_result_dict(self, pre_key, lr, rl, difference):
        result_key_value = {"lr": lr, "rl": rl, "difference": difference}
        if self.result.get(pre_key):
            # result_key_value.update(self.result.get(pre_key))
            self.result.get(pre_key)["lr"] = self.result.get(pre_key)['lr'].union(result_key_value['lr'])
            self.result.get(pre_key)['rl'] = self.result.get(pre_key)['rl'].union(result_key_value['rl'])
            self.result.get(pre_key)["difference"].update(result_key_value['difference'])
        else:
            self.result.update({pre_key: result_key_value})
        # print(str(self.result))

    def _flat_and_compare_dict(self, dict1, dict2):
        try:
            flat_dict_thread1 = threading.Thread(target=self._flatten_dict, args=(dict1,), name="flat_dict1")
            flat_dict_thread2 = threading.Thread(target=self._flatten_dict, args=(dict2,), name="flat_dict2")
            flat_dict_thread1.start()
            flat_dict_thread2.start()
            flat_dict_thread1.join()
            flat_dict_thread2.join()
        except IndexError as ie:
            raise IndexError("List object - {}".format(ie)).with_traceback(ke.__traceback__)
        flat_dict1, flat_dict2 = self.flat_dicts["flat_dict1"], self.flat_dicts["flat_dict2"]
        lr, rl, matched_keys = self._compare_keys(flat_dict1.keys(), flat_dict2.keys())
        difference = self._compare_dict(flat_dict1, flat_dict2, matched_keys)
        return lr, rl, difference

    def _compare_dict_with_list(self, dict1, dict2, pre_key):
        # print("_compare_dict_with_list | pre_key : " + pre_key)
        lr, rl, matched_keys = self._compare_keys(dict1.keys(), dict2.keys())
        self.update_result_dict(pre_key, lr, rl, dict())
        to_flat_dict1, to_flat_dict2 = dict(), dict()
        for matched_key in matched_keys:
            # print("_compare_dict_with_list | matched_key : " + matched_key)
            value1, value2 = dict1[matched_key], dict2[matched_key]
            if not value1 and not value2:
                continue
            if isinstance(value1, list) and isinstance(value1, list) == isinstance(value2, list):
                is_value_dict = isinstance(value1[0], dict) if value1 else isinstance(value2[0], dict)
                if is_value_dict:
                    # print("_compare_dict_with_list | type = list")
                    self._create_compare_sort_maps(value1, value2, matched_key, "{}__{}".format(pre_key, matched_key))
                else:
                    to_flat_dict1.update({matched_key: value1})
                    to_flat_dict2.update({matched_key: value2})
            elif isinstance(value1, dict) and isinstance(value1, dict) == isinstance(value2, dict) and self.does_dict_contain_list_value(value1):
                # print("_compare_dict_with_list | type = dict with list")
                self._compare_dict_with_list(value1, value2, "{}>>>{}".format(pre_key, matched_key))
            else:
                # print("4 : adding to flat")
                to_flat_dict1.update({matched_key: value1})
                to_flat_dict2.update({matched_key: value2})
        lr, rl, difference = self._flat_and_compare_dict(to_flat_dict1, to_flat_dict2)
        self.update_result_dict(pre_key, lr, rl, difference)

    def _compare_sort_maps(self, sort_map1, sort_map2, pre_key):
        # print("_compare_sort_maps | pre_key : " + pre_key)
        lr, rl, matched_sort_keys = self._compare_keys(sort_map1.keys(), sort_map2.keys())
        self.update_result_dict(pre_key, lr, rl, dict())
        for matched_sort_key in matched_sort_keys:
            # print("_compare_sort_maps | matched_sort_key : " + matched_sort_key)
            value1, value2 = sort_map1[matched_sort_key], sort_map2[matched_sort_key]
            if isinstance(value1, list) and isinstance(value1, list) == isinstance(value2, list) and value1 and value2:
                # print("_compare_dict_with_list | type = list")
                self._create_compare_sort_maps(value1, value2, "{}__{}".format(pre_key, matched_sort_key),
                                               matched_sort_key)
            elif isinstance(value1, dict) and isinstance(value1, dict) == isinstance(value2, dict):
                if self.does_dict_contain_list_value(value1):
                    # print("_compare_dict_with_list | type = dict with list")
                    self._compare_dict_with_list(value1, value2, "{} >>> {}".format(pre_key, matched_sort_key))
                else:
                    # print("1 : adding to flat")
                    lr, rl, difference = self._flat_and_compare_dict(value1, value2)
                    self.update_result_dict("{} >>> {}".format(pre_key, matched_sort_key), lr, rl, difference)

    def _compare_array_tables(self):
        sort_map1, sort_map2 = self.sort_maps["sort_map1"], self.sort_maps["sort_map2"]
        lr, rl, matched_keys = self._compare_keys(sort_map1.keys(), sort_map2.keys())
        self.update_result_dict("sort_keys", lr, rl, dict())
        field_lr, field_rl, matched_field_keys = self._compare_keys(self.fields1, self.fields2)
        self.update_result_dict("field_keys", field_lr, field_rl, dict())
        difference = dict()
        for matched_key in matched_keys:
            data1, data2 = sort_map1.get(matched_key), sort_map2.get(matched_key)
            for matched_field_key in matched_field_keys:
                # if matched_field_key in self.to_be_ignored:
                #     continue
                ind1, ind2 = self.fields1.index(matched_field_key), self.fields2.index(matched_field_key)
                value1, value2 = self.transform_value(data1[ind1]), self.transform_value(data2[ind2])
                if value1 != value2:
                    if isinstance(value1, float) or isinstance(value1, int):
                        if abs(value1 - value2) > float(self.threshold):
                            difference.update({matched_key + ":" + matched_field_key: "{}, {}".format(
                                str(value1).replace(",", ";"), str(value2).replace(",", ";"))})
                    else:
                        difference.update({matched_key + ":" + matched_field_key: "{}, {}".format(
                            str(value1).replace(",", ";").replace("\n", "").replace("\r", ""),
                            str(value2).replace(",", ";").replace("\n", "").replace("\r", ""))})

        self.update_result_dict("field_keys", field_lr, field_rl, difference)

    def _create_sort_maps(self, input_json, sort_key, fields=""):
        sort_map = dict()
        for obj in input_json:
            if self.is_array_table:
                comp_key = "#".join(key + ":" + str(obj[fields.index(key)]) for key in sort_key)
            else:
                comp_key = ";".join(key + ":" + str(self.__get_value(obj, key)) for key in sort_key)
            comp_key = comp_key.rstrip("\r\n")
            if comp_key in sort_map.keys():
                print("Duplicate Sort key - {} in {}".format(sort_key, comp_key))
                keys = sort_map.keys()
                duplicate = True
                count = 1
                while duplicate:
                    if comp_key + "_" + str(count) in keys:
                        count += 1
                    else:
                        comp_key = comp_key + "_" + str(count)
                        duplicate = False
            sort_map.update({comp_key: obj})
        self.sort_maps[threading.currentThread().getName()] = sort_map

    def _create_compare_sort_maps(self, json1, json2, sort_key, pre_key):
        self.sort_maps = dict()
        try:
            sort_key = self.sort_key_map[sort_key]
        except KeyError as ke:
            raise KeyError("Sort key expected for list - " + str(ke)).with_traceback(ke.__traceback__)
        if self.is_array_table:
            self.fields1 = [field["name"] for field in json1["fields"]]
            self.fields2 = [field["name"] for field in json2["fields"]]
            json1, json2 = json1["data"], json2["data"]
        else:
            self.fields1, self.fields2 = "", ""
        sort_map_thread1 = threading.Thread(target=self._create_sort_maps, args=(json1, sort_key, self.fields1),
                                            name="sort_map1")
        sort_map_thread2 = threading.Thread(target=self._create_sort_maps, args=(json2, sort_key, self.fields2),
                                            name="sort_map2")
        sort_map_thread1.start(); sort_map_thread2.start()
        sort_map_thread1.join(); sort_map_thread2.join()
        if self.thread_exceptions:
            raise Exception(str(self.thread_exceptions)).with_traceback(self.thread_exceptions.__traceback__)
        if self.is_array_table:
            self._compare_array_tables()
        else:
            self._compare_sort_maps(self.sort_maps["sort_map1"], self.sort_maps["sort_map2"], pre_key)

    def report_differences(self, diff_object, reporter):
        difference_keys = diff_object.keys()
        for difference_key in difference_keys:
            difference_value = diff_object.get(difference_key)
            reporter.write("\n" + difference_key + "," + self.transform_reporting_differences(difference_value))

    def report_json_differences(self, test_case_id, test_case_name, comparator_result, reporter):
        if not test_case_id or not test_case_name:
            reporter.write("Test Case ID and Name is good to have for reporting.")
        reporter.write("\n\nTestCase ID,Report,Result")
        reporter.write(str(test_case_id).replace(",", ";") + "," + test_case_name.replace(",", ";") + ",Fail")
        for result_key, result_value in comparator_result.items():
            lr = result_value["lr"]
            rl = result_value["rl"]
            difference_keys = (result_value["difference"]).keys()
            if lr or rl or difference_keys:
                reporter.write("\n\n,Key : " + self.transform_reporting_differences(result_key))
            if lr or rl:
                reporter.write("\nJSON1(keys) - JSON2(keys),JSON2(keys) - JSON1(keys)\n")
                reporter.write(self.transform_reporting_differences(
                    result_value["lr"]) + "," + self.transform_reporting_differences(result_value["rl"]))
            if difference_keys:
                reporter.write("\nKey,Value1,Value2\n")
            self.report_differences(result_value["difference"], reporter)

    @staticmethod
    def _get_list_type(json_list):
        list_type = None
        for json_item in json_list:
            if json_item is not None:
                if list_type:
                    if list_type != str(type(json_item)):
                        return
                else:
                    list_type = str(type(json_item))
        return list_type

    def compare_primitive_jsons(self, json1, json2):
        status = json1 == json2
        if isinstance(json1, float) or isinstance(json1, int):
            status = abs(json1 - json2) <= float(self.threshold)
        result = {} if status else "Values '{}' and '{}' do not match".format(json1, json2)
        return [status, result, 0]

    @staticmethod
    def create_sort_map_primitive_lists(input_json):
        sort_map = dict()
        for j in input_json:
            if j in sort_map.keys():
                count = sort_map.get(j) + 1
                sort_map.update({j: count})
            else:
                sort_map.update({j: 1})
        return sort_map

    def compare_primitive_lists(self, json1, json2):
        lr, rl = list(), list()
        sort_map = self.create_sort_map_primitive_lists(json1)
        for j in json2:
            if j in sort_map.keys():
                count = sort_map.get(j) - 1
                if count >= 0:
                    sort_map.update({j: count})
                else:
                    rl.append(j)
            else:
                rl.append(j)
        for key, values in sort_map.items():
            for i in range(0, values):
                lr.append(key)
        return lr, rl

    def compare_json(self, json1=None, json2=None):
        start_time = datetime.now()
        json1_type, json2_type = str(type(json1)), str(type(json2))
        if json1_type != json2_type:
            return [False, str("JSONs are not of the same type (" + json1_type + " --- " + json2_type + ")"), 0]
        if isinstance(json1, bool) or isinstance(json1, int) or isinstance(json1, str) or isinstance(json1, float):
            return self.compare_primitive_jsons(json1, json2)
        if not json1 and not json2:
            return [True, "JSONs are empty", 0]
        if not json1 or not json2:
            return [False, "One of the JSONs is empty. Nothing to be compared.", 0]
        if not isinstance(json1, list) and not isinstance(json1, dict):
            return [False, "JSONs are not a proper json object. Nothing to be compared.", 0]
        if isinstance(json1, dict) and json1.get("@CLASS") == "com.google.common.collect.Table":
            self.is_array_table = True
            self._create_compare_sort_maps(json1, json2, "data", "base")
        elif isinstance(json1, list):
            list_type1 = self._get_list_type(json1)
            list_type2 = self._get_list_type(json2)
            if list_type1 != list_type2:
                return [False, "JSONs are lists of different types", 0]
            if not list_type1:
                return [False, "JSONs are heterogeneous lists. This is not supported currently.", 0]
            if list_type1 == "<class 'dict'>":
                self._create_compare_sort_maps(json1, json2, "base", "base")
            else:
                lr, rl = self.compare_primitive_lists(json1, json2)
                self.result.update({"base": {"lr": lr, "rl": rl, "difference": dict()}})
        else:
            if self.does_dict_contain_list_value(json1):
                self._compare_dict_with_list(json1, json2, "base")
            else:
                lr, rl, difference = self._flat_and_compare_dict(json1, json2)
                self.result.update({"base": {"lr": lr, "rl": rl, "difference": difference}})
        status = True
        for result_key, result_value in self.result.items():
            if result_value["lr"] or result_value["rl"] or result_value["difference"]:
                status = False
                break
        self.comparison_time = (datetime.now() - start_time).total_seconds()
        if status:
            return [status, dict(), self.comparison_time]
        if self.reporter:
            self.report_json_differences(self.test_case_id, self.test_case_name, self.result, self.reporter)
        return [status, self.result, self.comparison_time]
