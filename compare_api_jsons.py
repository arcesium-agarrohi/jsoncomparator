import json
import requests
import JsonComparator
def compare_json_objects(obj1, obj2):
    # Check if both objects are dictionaries
    if isinstance(obj1, dict) and isinstance(obj2, dict):
        # Check if the keys are the same
        if set(obj1.keys()) != set(obj2.keys()):
            # print("K1: " + str(set(obj1.keys())) + "  K2: " + str(set(obj1.keys())) + "  obj1: " + str(obj1))
            return False
        # Recursively compare the values
        for key in obj1:
            if key != "@id" and not compare_json_objects(obj1[key], obj2[key]):
                # print("compare_json_objects K1: " + str(set(obj1.keys())) + "  K2: " + str(set(obj1.keys())) + "  obj1: " + str(obj1))
                return False
        return True

    # Check if both objects are lists
    elif isinstance(obj1, list) and isinstance(obj2, list):
        # Check if the lists are the same length
        if len(obj1) != len(obj2):
            # print("len K1: " + str((obj1)) + "  K2: " + str((obj1)) + "  obj1: " + str(obj1))
            return False
        # Recursively compare the elements
        for i in range(len(obj1)):
            if not compare_json_objects(obj1[i], obj2[i]):
                # print("len compare_json_objects K1: " + str((obj1)) + "  K2: " + str((obj1)) + "  obj1: " + str(obj1))
                return False
        return True

    # Compare primitive types directly (strings, numbers, booleans, None)
    else:
        return obj1 == obj2

def compare_json(json_response1, json_response2):
    # Parse JSON responses into dictionaries
    response1_dict = json.loads(json_response1)
    response2_dict = json.loads(json_response2)

    # Compare the parsed JSON objects
    if compare_json_objects(response1_dict, response2_dict):
        print("The JSON responses are equal.")
    else:
        print("The JSON responses are not equal.")
        # print(response1_dict)
        # print(response2_dict)

# Investment thread apis testing -
master_fund_ids = ["464", "null", "null", "null",
                   "464", "464", "464", "464", "462", "462", "462", "462", "67", "67", "67", "67", "18", "18", "18", "18", "449", "449", "449", "449", "447", "447", "447", "447",
                   "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null",
                   "481", "481", "481", "481", "481", "481", "481", "481", "484", "484", "484", "484", "483", "483", "483", "483",
                   "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "484", "484", "484", "481", "null", "null", "null", "483", "483", "483"]

master_account_ids = ["null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null",
                      "null", "null", "null", "null", "null", "null", "8734", "8734", "8734", "8734", "8750", "8750", "8750", "8750", "8748", "8748", "8478", "8748", "7658", "7658", "7658", "7658",
                      "8734", "8734", "8734", "8734", "8750", "8750", "8750", "8750", "8750", "8750", "8750", "8750", "8748", "8748", "8748", "8748",
                      "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "8750", "8750", "8750", "8734", "null", "null", "null", "null", "8748", "8748"]

investment_thread_ids = ["null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null",
                         "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null",
                         "null", "null", "null", "null", "null",
                         "168", "179", "166, 179,188,71", "168", "168", "168", "179", "179", "168,71", "168,1,71", "168,1,71", "179", "179", "168", "179", "166,178", "166,178", "166,178", "166,178", "166,178", "166,177,178"]

dates = ["20210331", "null", "20210228", "20210301",
         "null", "20210301", "20210315", "20210331", "null", "20210301", "20210315", "20210331",
         "null", "20220301", "20220315", "20220331", "null", "20220301", "20220315", "20220331", "null", "20180301", "20180315", "20180331", "null", "20180301", "20180315", "20180331",
         "null", "20221001", "20221015", "20221031", "null", "20221001", "20221015", "20221031", "null", "20221001", "20221015", "20221031", "null", "20210301", "20210315", "20230331",
         "null", "20221001", "20221015", "20221031", "null", "20221001", "20221015", "20221031", "null", "20221001", "20221015", "20221031", "null", "20221001", "20221015", "20221031",
         "null", "null", "null", "null", "20220901", "20220921", "20220901", "20220921", "20220921", "20210301", "20210401", "20220901", "20220921", "20221001", "20220901", "20220901", "20220921", "20221001", "20221001", "20221001", "20221001"
         ]

s = len(dates)

for i in range(s):

    url1 = "http://u-agarrohi-descaphf-dev.sd.deshaw.com:8200/fuma/webservices/investmentThreadManager/getInvestmentThread?masterFundID=#master_fund_id#&masterAccountID=#master_account_id#&date=#date#&investmentThreadIDList=#investment_thread_id_list#&inputFormat=json&format=json"
    url2 = "https://descapdev.deshaw.com/hf/webservices/investmentThreadManager/getInvestmentThread?masterFundID=#master_fund_id#&masterAccountID=#master_account_id#&date=#date#&investmentThreadIDList=#investment_thread_id_list#&inputFormat=json&format=json"

    # print(i)
    master_fund_id = master_fund_ids[i]
    master_account_id = master_account_ids[i]
    date = dates[i]
    investment_thread_id = investment_thread_ids[i]
    print("MasterFundID: " + str(master_fund_id) + ", MasterAccountID: " + str(master_account_id) + ", InvestmentThreadID: " + str(investment_thread_id) + ", Date: " + str(date))

    url1 = url1.replace("#master_fund_id#", master_fund_id).replace("#master_account_id#", master_account_id).replace("#date#", date).replace("#investment_thread_id_list#", investment_thread_id)
    url2 = url2.replace("#master_fund_id#", master_fund_id).replace("#master_account_id#", master_account_id).replace("#date#", date).replace("#investment_thread_id_list#", investment_thread_id)

    api1 = requests.get(url1)
    api2 = requests.get(url2)

    api1_array = api1.json()
    api2_array = api2.json()

    api1_array.sort(key=lambda x: x["investmentThreadID"])
    api2_array.sort(key=lambda x: x["investmentThreadID"])

    object = JsonComparator.JsonComparator()
    print(object.compare_json(json.dumps(api1_array), json.dumps(api2_array)))



    # compare_json(json.dumps(api1_array), json.dumps(api2_array))
# compare_json(api1.json(), api2.json())



