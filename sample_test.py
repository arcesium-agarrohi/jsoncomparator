import json
import requests
import JsonComparator
def compare_json_objects(obj1, obj2):
    # Check if both objects are dictionaries
    if isinstance(obj1, dict) and isinstance(obj2, dict):
        # Check if the keys are the same
        if set(obj1.keys()) != set(obj2.keys()):
            # print("K1: " + str(set(obj1.keys())) + "  K2: " + str(set(obj1.keys())) + "  obj1: " + str(obj1))
            return False
        # Recursively compare the values
        for key in obj1:
            if key != "@id" and not compare_json_objects(obj1[key], obj2[key]):
                # print("compare_json_objects K1: " + str(set(obj1.keys())) + "  K2: " + str(set(obj1.keys())) + "  obj1: " + str(obj1))
                return False
        return True

    # Check if both objects are lists
    elif isinstance(obj1, list) and isinstance(obj2, list):
        # Check if the lists are the same length
        if len(obj1) != len(obj2):
            # print("len K1: " + str((obj1)) + "  K2: " + str((obj1)) + "  obj1: " + str(obj1))
            return False
        # Recursively compare the elements
        for i in range(len(obj1)):
            if not compare_json_objects(obj1[i], obj2[i]):
                # print("len compare_json_objects K1: " + str((obj1)) + "  K2: " + str((obj1)) + "  obj1: " + str(obj1))
                return False
        return True

    # Compare primitive types directly (strings, numbers, booleans, None
    else:
        return obj1 == obj2

def compare_json(json_response1, json_response2):
    # Parse JSON responses into dictionaries
    response1_dict = json.loads(json_response1)
    response2_dict = json.loads(json_response2)

    # Compare the parsed JSON objects
    if compare_json_objects(response1_dict, response2_dict):
        print("The JSON responses are equal.")
    else:
        print("The JSON responses are not equal.")
        # print(response1_dict)
        # print(response2_dict)

# Example JSON responses with nested structures (Response of getInvestmentThread Api)
response1 = [{'name': 'John', 'age': 30, 'address': {'city': 'New York', 'zip': 10001, 'namespace': {'pod':'deshawuat'}}}]
response2 = [{'age': 30, 'name': 'John', 'address': {'zip': 10001, 'namespace': {'pod':'deshawuat'}, 'city': 'New York'}}]
response3 = [{'name': 'Alice', 'age': 25, 'address': {'city': 'Los Angeles', 'zip': 90001}}]

# Compare responses
compare_json(json.dumps(response1), json.dumps(response2))  # The JSON responses are equal.
compare_json(json.dumps(response1), json.dumps(response3))  # The JSON responses are not equal.

# Example JSON response lists with nested structures
# print("JSON list comparison")
# response1 = [{"age": 30, "name": "John", "address": {"city": "New York", "zip": 10001, "namespace": {"pod":"deshawuat"}}},{"city": "New York", "zip": 10001, "namespace": {"pod":"deshawuat"}}]
# response2 = [{"city": "New York", "zip": 10001, "namespace": {"pod":"deshawuat"}}, {"name": "John", "age": 30, "address": {"zip": 10001, "city": "New York", "namespace": {"pod":"deshawuat"}}}]
# response3 = '{"name": "Alice", "age": 25, "address": {"city": "Los Angeles", "zip": 90001}}'
# response4 = '{"age": 25, "name": "Alice", "address": {"city": "Los Angeles", "zip": 90001}}'


object = JsonComparator.JsonComparator()
print(object.compare_json(json.dumps(response1), json.dumps(response2)))




# Compare responses
# print(response1)
# print(json.dumps(response1.json()))
# for i in (0,s):
#     compare_json(json.dumps(api1.json()), json.dumps(api2.json()))

# Investment thread apis testing -
# api1 = requests.get("http://u-agarrohi-descaphf-dev.sd.deshaw.com:8200/fuma/webservices/investmentThreadManager/getInvestmentThread?masterFundID=null&masterAccountID=null&date=20220922&investmentThreadIDList=168,71,1&inputFormat=json&format=json")
# api2 = requests.get("https://descapdev.deshaw.com/hf/webservices/investmentThreadManager/getInvestmentThread?masterFundID=null&masterAccountID=null&date=20220922&investmentThreadIDList=1,168,71&inputFormat=json&format=json")

# s = len(api1.json())
# print(s)
# api1_array_before_sorted = api1.json()
# print("Before sorted")
# # print(api1_array_before_sorted)
# api1_array = api1_array_before_sorted
# api1_array.sort(key=lambda x: x["investmentThreadID"])
# print("After sorted" )
# # print(api1_array)
#
# api2_array = api2.json()
# # print(api2_array)
# api2_array.sort(key=lambda x: x["investmentThreadID"])
#

# print(api2_array)
